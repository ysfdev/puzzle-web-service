const express = require('express')
const app = express()
const router = require('./router');
const port = process.env.PORT || 3000;

//Mount our router
app.use('/', router);

//Start server
app.listen(port, function () {
  console.log(`Service app running on http://localhost:${port}`);
})