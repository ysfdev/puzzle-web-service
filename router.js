const express = require('express');
const router = express.Router();
const puzzleSolver = require('./lib/puzzle-solver');

//Log all requests
router.use(function (req, res, next) {
  console.log('New request received:', req.originalUrl);
  next();
})

router.get('/', function (req, res)  {
    const q = req.query.q;
    const d = req.query.d;
    //Update the headers conten-type to text/plain
    res.set('Content-Type', 'text/plain');
    //Check the request query received and send appropiate response
    switch (q) {
      case 'Ping':
        res.send('OK');
        break;
      case 'Status':
        res.send('Yes. Green Card');
        break;
      case 'Phone':
        res.send('6464744068');
        break;
      case 'Years':
        res.send('2+');
        break;
      case 'Source':
        res.send('Project source located at: https://bitbucket.org/ysfdev/puzzle-web-service');
        break;
      case 'Degree':
        res.send('Bachelor of Computer Information Systems');
        break;
      case 'Email Address':
        res.send('ysantana.developer@gmail.com');
        break;
      case 'Resume':
        res.send('https://drive.google.com/open?id=0B4ZB0-PXgJtGdlNkSW14c3lkcExDNl84RUZQMmdrS1JmZzRz');
        break;
      case 'Name':
        res.send('Yeramin Santana');
        break;
      case 'Position':
        res.send('Full Stack Developer');
        break;
      case 'Referrer':
        res.send('Atlantic Group, Inc');
        break;
      case 'Puzzle':
        let da = decodeURI(d);
        let puzzleStr = da.split(':')[1].split(" ")[1];
        let solvedPuzzle = puzzleSolver.solvePuzzle(puzzleStr);
        res.send(solvedPuzzle);
        break;
      default:
        res.send('Unknown query');
        break;
    }
});

/** 
 *
 * ERROR HANDLING
 *
 */

router.use(function (req, res, next) {
  console.error(`Requested URL not found. URL: ${req.url}`)
  res.status(404).send('The URL requested was not found');
});

router.use(function (err, req, res, next) {
  console.error(`Internal server error occured. Error stack: ${err.stack || err}`)
  res.status(500).send('Internal Server Error!');
});

module.exports = router;