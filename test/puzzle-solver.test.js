const puzzleSolver = require('../lib/puzzle-solver');
const expect = require('chai').expect;

describe('Puzzle Solver ', function() {
    const puzzleStr = 'ABCD\nA->--\nB-=--\nC>---\nD-<--\n';
    const solvedStr = puzzleSolver.solvePuzzle(puzzleStr);
    it('should solve puzzle1 with missing valid chars', function() {
        let solution = "ABCD A=><> B<=<> C>>=> D<<<=";
        expect(solvedStr).equal(solution);
    });

    it('should solve puzzle2 with missing valid chars', function() {
        const puzzleStr2 = 'ABCD\nA->--\nB-->-\nC--=-\nD--<-';
        const solvedStr2 = puzzleSolver.solvePuzzle(puzzleStr2);
        let solution2 = "ABCD A=>>> B<=>> C<<=> D<<<=";
        expect(solvedStr2).equal(solution2);
    });
});