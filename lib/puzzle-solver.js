/**
 * Solve puzzle ABCD of given str by finding the missing values of (< = >) on on empty lines with - char
 * @param {String} puzzleStr puzzle string value to insert values to. Ex: "ABCD A=--- B---> C<--- D>---"
 * @return {String} - solved puzzle str with filled missing chars
 */

 exports.solvePuzzle = (puzzleStr) => {
    let strValues = puzzleStr.split(/\ABCD/)[1].split("\n").filter((c) => c !== '');
    let letterChars = [];

    //Get the unique given char from each letter 
    strValues.forEach((ch, idx) => {  
        let chars = ch.split("");
        let letterName = chars[0];
        let values = chars.slice(1);
        let uniqueChar = values.find((ch) => ch !== '-');
        let uniqueCharIdx = values.indexOf(uniqueChar);
        letterChars.push({letterName, values, uniqueChar, uniqueCharIdx});
    })

    return formatSolvedLetters(solveMissingLetterChars(letterChars));
 };

/**
 * Solve the missing chars for the given letterChars
 * @param {Array} letterChars 
 */
 const solveMissingLetterChars = (letterChars) => {
    const oppositeChar = {
        '>' : '<',
        '<' : '>'
    };

    let solvedLetters = [];
    letterChars.forEach((currentLetter, letterIdx) => {
        let updatedLetterChars = [];
        //Generated the missing chars for this letter
        for (let idx = 0; idx < 4; idx++) {
           let char = "";
           if (idx === letterIdx) char = '=';
           else if (idx === currentLetter.uniqueCharIdx) char = currentLetter.uniqueChar;
           else char = (letterChars[idx].uniqueChar === '=') ? currentLetter.uniqueChar : oppositeChar[letterChars[idx].uniqueChar];
           updatedLetterChars.push(char);
        }
        let solvedLetter = `${currentLetter.letterName}${updatedLetterChars.toString().replace(/\,/gi, '')}`; 
        solvedLetters.push(solvedLetter);
    })
    return solvedLetters;
 };

 /**
  * Format the solved letters to the expected output
  * @param {Array} solvedLetters 
  */
 const formatSolvedLetters = (solvedLetters) => {
   return `ABCD ${solvedLetters[0]} ${solvedLetters[1]} ${solvedLetters[2]} ${solvedLetters[3]}`;
 };