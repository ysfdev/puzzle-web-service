## Web Service 
Send assesments responses based on appropiate requests queries.

## Setup server
To install the server dependencies please run: `npm install`

## Run tests
To run tests for puzzle solver run: `npm test`;

## Start Sercie
To start the service. Please run `npm start`. The app will start listen on localhost:3000.